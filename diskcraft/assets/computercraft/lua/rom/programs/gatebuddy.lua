--[[  LanteaCraft      ]]--
--[[      and SGCraft  ]]--
--[[    Gate Buddy     ]]--
--[[      by Dog       ]]--
--[[ aka HydrantHunter ]]--
--[[ pastebin B81kt39c ]]--
local gbVer = "2.0.00"
--[[
Tested with/requires:
  - Minecraft 1.7.10+ AND ComputerCraft 1.75+ || LanteaCraft LC2-16+ | OR | SGCraft1.11.x-mc1.7.10+
  For setup and usage instructions, please visit http://tinyurl.com/jf3rjr7

Special thanks to: SquidDev     (AES encryption/decryption)
                   Alex Kloss   (base64 encoder/decoder)

IMPORTANT NOTE:
  - all of the following variables are set by the program as necessary
  - editing any of them below will most likely cause unexpected results
]]--
local tArgs, clients = { ... }, { }
local gate, thisGate, dialAddress, pingTimer
local chevronNumber, clientCount, thisCC = 0, 0, tostring(os.getComputerID())
local modemSide, callDirection, gateStatus, remoteIrisStatus = "none", "none", "QRY", "unk"
local lcGate, continueDialing, spinTime, irisState, allowIrisClose, logging = false, false, false, false, false, false
local sgStates = {
  Idle = "Idle";
  Dialing = "Dialing";
  Dialling = "Dialing";
  Opening = "Dialing";
  Connected = "Connected";
  Closing = "Disconnecting";
  Offline = "Offline";
}

-- Lua 5.1+ base64 v3.0 (c) 2009 by Alex Kloss <alexthkloss@web.de>
-- licensed under the terms of the LGPL2
-- http://lua-users.org/wiki/BaseSixtyFour
-- character table string
local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
-- encoding
local function encode(data)
  return ((data:gsub('.', function(x) 
    local r,b='',x:byte()
    for i=8,1,-1 do r=r..(b%2^i-b%2^(i-1)>0 and '1' or '0') end
    return r;
  end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
    if (#x < 6) then return '' end
    local c=0
    for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
    return b:sub(c+1,c+1)
  end)..({ '', '==', '=' })[#data%3+1])
end
-- decoding
local function decode(data)
  data = string.gsub(data, '[^'..b..'=]', '')
  return (data:gsub('.', function(x)
    if (x == '=') then return '' end
    local r,f='',(b:find(x)-1)
    for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
    return r;
  end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
    if (#x ~= 8) then return '' end
    local c=0
    for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
    return string.char(c)
  end))
end

-- AES Lua implementation by SquidDev
-- https://gist.github.com/SquidDev/86925e07cbabd70773e53d781bd8b2fe
-- http://pastebin.com/DMx8M0LP
local encrypt, decrypt
do
  local function _W(f) local e=setmetatable({}, {__index = _ENV or getfenv()}) if setfenv then setfenv(f, e) end return f(e) or e end
  local bit=_W(function(_ENV, ...)
  --[[
    This bit API is designed to cope with unsigned integers instead of normal integers
    To do this we add checks for overflows: (x > 2^31 ? x - 2 ^ 32 : x)
    These are written in long form because no constant folding.
  ]]
  local floor = math.floor
  local lshift, rshift

  rshift = function(a,disp)
    return floor(a % 4294967296 / 2^disp)
  end

  lshift = function(a,disp)
    return (a * 2^disp) % 4294967296
  end

  return {
    -- bit operations
    bnot = bit32 and bit32.bnot or bit.bnot,
    band = bit32 and bit32.band or bit.band,
    bor  = bit32 and bit32.bor or bit.bor,
    bxor = bit32 and bit32.bxor or bit.bxor,
    rshift = rshift,
    lshift = lshift,
  }
  end)

  local gf=_W(function(_ENV, ...)
  -- finite field with base 2 and modulo irreducible polynom x^8+x^4+x^3+x+1 = 0x11d
  local bxor = bit32 and bit32.bxor or bit.bxor
  local lshift = bit.lshift
  -- private data of gf
  local n = 0x100
  local ord = 0xff
  local irrPolynom = 0x11b
  local exp, log = {}, {}
  --
  -- add two polynoms (its simply xor)
  --
  local function add(operand1, operand2)
    return bxor(operand1,operand2)
  end
  --
  -- subtract two polynoms (same as addition)
  --
  local function sub(operand1, operand2)
    return bxor(operand1,operand2)
  end
  --
  -- inverts element
  -- a^(-1) = g^(order - log(a))
  --
  local function invert(operand)
    -- special case for 1 or normal invert
    return operand == 1 and 1 or exp[ord - log[operand]]
  end
  --
  -- multiply two elements using a logarithm table
  -- a*b = g^(log(a)+log(b))
  --
  local function mul(operand1, operand2)
    if (operand1 == 0 or operand2 == 0) then
      return 0
    end
    local exponent = log[operand1] + log[operand2]
    if (exponent >= ord) then
      exponent = exponent - ord
    end
    return exp[exponent]
  end
  --
  -- divide two elements
  -- a/b = g^(log(a)-log(b))
  --
  local function div(operand1, operand2)
    if (operand1 == 0)  then
      return 0
    end
    -- TODO: exception if operand2 == 0
    local exponent = log[operand1] - log[operand2]
    if (exponent < 0) then
      exponent = exponent + ord
    end
    return exp[exponent]
  end
  --
  -- print logarithmic table
  --
  local function printLog()
    for i = 1, n do
      print("log(", i-1, ")=", log[i-1])
    end
  end
  --
  -- print exponentiation table
  --
  local function printExp()
    for i = 1, n do
      print("exp(", i-1, ")=", exp[i-1])
    end
  end
  --
  -- calculate logarithmic and exponentiation table
  --
  local function initMulTable()
    local a = 1
    for i = 0,ord-1 do
      exp[i] = a
      log[a] = i
      -- multiply with generator x+1 -> left shift + 1
      a = bxor(lshift(a, 1), a)
      -- if a gets larger than order, reduce modulo irreducible polynom
      if a > ord then
        a = sub(a, irrPolynom)
      end
    end
  end

  initMulTable()

  return {
    add = add,
    sub = sub,
    invert = invert,
    mul = mul,
    div = div,
    printLog = printLog,
    printExp = printExp,
  }
  end)

  util=_W(function(_ENV, ...)
  -- Cache some bit operators
  local bxor = bit.bxor
  local rshift = bit.rshift
  local band = bit.band
  local lshift = bit.lshift
  local sleepCheckIn
  --
  -- calculate the parity of one byte
  --
  local function byteParity(byte)
    byte = bxor(byte, rshift(byte, 4))
    byte = bxor(byte, rshift(byte, 2))
    byte = bxor(byte, rshift(byte, 1))
    return band(byte, 1)
  end
  --
  -- get byte at position index
  --
  local function getByte(number, index)
    return index == 0 and band(number,0xff) or band(rshift(number, index*8),0xff)
  end
  --
  -- put number into int at position index
  --
  local function putByte(number, index)
    return index == 0 and band(number,0xff) or lshift(band(number,0xff),index*8)
  end
  --
  -- convert byte array to int array
  --
  local function bytesToInts(bytes, start, n)
    local ints = {}
    for i = 0, n - 1 do
      ints[i + 1] =
          putByte(bytes[start + (i*4)], 3) +
          putByte(bytes[start + (i*4) + 1], 2) +
          putByte(bytes[start + (i*4) + 2], 1) +
          putByte(bytes[start + (i*4) + 3], 0)
      if n % 10000 == 0 then sleepCheckIn() end
    end
    return ints
  end
  --
  -- convert int array to byte array
  --
  local function intsToBytes(ints, output, outputOffset, n)
    n = n or #ints
    for i = 0, n - 1 do
      for j = 0,3 do
        output[outputOffset + i*4 + (3 - j)] = getByte(ints[i + 1], j)
      end
      if n % 10000 == 0 then sleepCheckIn() end
    end
    return output
  end
  --
  -- convert bytes to hexString
  --
  local function bytesToHex(bytes)
    local hexBytes = ""
    for i,byte in ipairs(bytes) do
      hexBytes = hexBytes .. string.format("%02x ", byte)
    end
    return hexBytes
  end

  local function hexToBytes(bytes)
    local out = {}
    for i = 1, #bytes, 2 do
      out[#out + 1] = tonumber(bytes:sub(i, i + 1), 16)
    end
    return out
  end
  --
  -- convert data to hex string
  --
  local function toHexString(data)
    local type = type(data)
    if (type == "number") then
      return string.format("%08x",data)
    elseif (type == "table") then
      return bytesToHex(data)
    elseif (type == "string") then
      local bytes = {string.byte(data, 1, #data)}
      return bytesToHex(bytes)
    else
      return data
    end
  end

  local function padByteString(data)
    local dataLength = #data
    local random1 = math.random(0,255)
    local random2 = math.random(0,255)
    local prefix = string.char(random1,
      random2,
      random1,
      random2,
      getByte(dataLength, 3),
      getByte(dataLength, 2),
      getByte(dataLength, 1),
      getByte(dataLength, 0)
    )
    data = prefix .. data
    local padding, paddingLength = "", math.ceil(#data/16)*16 - #data
    for i=1,paddingLength do
      padding = padding .. string.char(math.random(0,255))
    end
    return data .. padding
  end

  local function properlyDecrypted(data)
    local random = {string.byte(data,1,4)}
    if (random[1] == random[3] and random[2] == random[4]) then
      return true
    end
    return false
  end

  local function unpadByteString(data)
    if (not properlyDecrypted(data)) then
      return nil
    end
    local dataLength = putByte(string.byte(data,5), 3)
             + putByte(string.byte(data,6), 2)
             + putByte(string.byte(data,7), 1)
             + putByte(string.byte(data,8), 0)
    return string.sub(data,9,8+dataLength)
  end

  local function xorIV(data, iv)
    for i = 1,16 do
      data[i] = bxor(data[i], iv[i])
    end
  end

  local function increment(data)
    local i = 16
    while true do
      local value = data[i] + 1
      if value >= 256 then
        data[i] = value - 256
        i = (i - 2) % 16 + 1
      else
        data[i] = value
        break
      end
    end
  end

  -- Called every encryption cycle
  local push, pull, time = os.queueEvent, coroutine.yield, os.time
  local oldTime = time()
  local function sleepCheckIn()
    local newTime = time()
    if newTime - oldTime >= 0.03 then -- (0.020 * 1.5)
      oldTime = newTime
      push("sleep")
      pull("sleep")
    end
  end

  local function getRandomData(bytes)
    local char, random, sleep, insert = string.char, math.random, sleepCheckIn, table.insert
    local result = {}
    for i=1,bytes do
      insert(result, random(0,255))
      if i % 10240 == 0 then sleep() end
    end
    return result
  end

  local function getRandomString(bytes)
    local char, random, sleep, insert = string.char, math.random, sleepCheckIn, table.insert
    local result = {}
    for i=1,bytes do
      insert(result, char(random(0,255)))
      if i % 10240 == 0 then sleep() end
    end
    return table.concat(result)
  end

  return {
    byteParity = byteParity,
    getByte = getByte,
    putByte = putByte,
    bytesToInts = bytesToInts,
    intsToBytes = intsToBytes,
    bytesToHex = bytesToHex,
    hexToBytes = hexToBytes,
    toHexString = toHexString,
    padByteString = padByteString,
    properlyDecrypted = properlyDecrypted,
    unpadByteString = unpadByteString,
    xorIV = xorIV,
    increment = increment,
    sleepCheckIn = sleepCheckIn,
    getRandomData = getRandomData,
    getRandomString = getRandomString,
  }
  end)

  aes=_W(function(_ENV, ...)
  -- Implementation of AES with nearly pure lua
  -- AES with lua is slow, really slow :-)
  local putByte = util.putByte
  local getByte = util.getByte
  -- some constants
  local ROUNDS = 'rounds'
  local KEY_TYPE = "type"
  local ENCRYPTION_KEY=1
  local DECRYPTION_KEY=2
  -- aes SBOX
  local SBox = {}
  local iSBox = {}
  -- aes tables
  local table0 = {}
  local table1 = {}
  local table2 = {}
  local table3 = {}
  local tableInv0 = {}
  local tableInv1 = {}
  local tableInv2 = {}
  local tableInv3 = {}
  -- round constants
  local rCon = {
    0x01000000,
    0x02000000,
    0x04000000,
    0x08000000,
    0x10000000,
    0x20000000,
    0x40000000,
    0x80000000,
    0x1b000000,
    0x36000000,
    0x6c000000,
    0xd8000000,
    0xab000000,
    0x4d000000,
    0x9a000000,
    0x2f000000,
  }
  --
  -- affine transformation for calculating the S-Box of AES
  --
  local function affinMap(byte)
    mask = 0xf8
    result = 0
    for i = 1,8 do
      result = bit.lshift(result,1)
      parity = util.byteParity(bit.band(byte,mask))
      result = result + parity
      -- simulate roll
      lastbit = bit.band(mask, 1)
      mask = bit.band(bit.rshift(mask, 1),0xff)
      mask = lastbit ~= 0 and bit.bor(mask, 0x80) or bit.band(mask, 0x7f)
    end
    return bit.bxor(result, 0x63)
  end
  --
  -- calculate S-Box and inverse S-Box of AES
  -- apply affine transformation to inverse in finite field 2^8
  --
  local function calcSBox()
    for i = 0, 255 do
      inverse = i ~= 0 and gf.invert(i) or 0
      mapped = affinMap(inverse)
      SBox[i] = mapped
      iSBox[mapped] = i
    end
  end
  --
  -- Calculate round tables
  -- round tables are used to calculate shiftRow, MixColumn and SubBytes
  -- with 4 table lookups and 4 xor operations.
  --
  local function calcRoundTables()
    for x = 0,255 do
      byte = SBox[x]
      table0[x] = putByte(gf.mul(0x03, byte), 0)
                + putByte(             byte , 1)
                + putByte(             byte , 2)
                + putByte(gf.mul(0x02, byte), 3)
      table1[x] = putByte(             byte , 0)
                + putByte(             byte , 1)
                + putByte(gf.mul(0x02, byte), 2)
                + putByte(gf.mul(0x03, byte), 3)
      table2[x] = putByte(             byte , 0)
                + putByte(gf.mul(0x02, byte), 1)
                + putByte(gf.mul(0x03, byte), 2)
                + putByte(             byte , 3)
      table3[x] = putByte(gf.mul(0x02, byte), 0)
                + putByte(gf.mul(0x03, byte), 1)
                + putByte(             byte , 2)
                + putByte(             byte , 3)
    end
  end
  --
  -- Calculate inverse round tables
  -- does the inverse of the normal roundtables for the equivalent
  -- decryption algorithm.
  --
  local function calcInvRoundTables()
    for x = 0,255 do
      byte = iSBox[x]
      tableInv0[x] = putByte(gf.mul(0x0b, byte), 0)
                 + putByte(gf.mul(0x0d, byte), 1)
                 + putByte(gf.mul(0x09, byte), 2)
                 + putByte(gf.mul(0x0e, byte), 3)
      tableInv1[x] = putByte(gf.mul(0x0d, byte), 0)
                 + putByte(gf.mul(0x09, byte), 1)
                 + putByte(gf.mul(0x0e, byte), 2)
                 + putByte(gf.mul(0x0b, byte), 3)
      tableInv2[x] = putByte(gf.mul(0x09, byte), 0)
                 + putByte(gf.mul(0x0e, byte), 1)
                 + putByte(gf.mul(0x0b, byte), 2)
                 + putByte(gf.mul(0x0d, byte), 3)
      tableInv3[x] = putByte(gf.mul(0x0e, byte), 0)
                 + putByte(gf.mul(0x0b, byte), 1)
                 + putByte(gf.mul(0x0d, byte), 2)
                 + putByte(gf.mul(0x09, byte), 3)
    end
  end
  --
  -- rotate word: 0xaabbccdd gets 0xbbccddaa
  -- used for key schedule
  --
  local function rotWord(word)
    local tmp = bit.band(word,0xff000000)
    return (bit.lshift(word,8) + bit.rshift(tmp,24))
  end
  --
  -- replace all bytes in a word with the SBox.
  -- used for key schedule
  --
  local function subWord(word)
    return putByte(SBox[getByte(word,0)],0)
      + putByte(SBox[getByte(word,1)],1)
      + putByte(SBox[getByte(word,2)],2)
      + putByte(SBox[getByte(word,3)],3)
  end
  --
  -- generate key schedule for aes encryption
  --
  -- returns table with all round keys and
  -- the necessary number of rounds saved in [ROUNDS]
  --
  local function expandEncryptionKey(key)
    local keySchedule = {}
    local keyWords = math.floor(#key / 4)
    if ((keyWords ~= 4 and keyWords ~= 6 and keyWords ~= 8) or (keyWords * 4 ~= #key)) then
      error("Invalid key size: " .. tostring(keyWords))
      return nil
    end
    keySchedule[ROUNDS] = keyWords + 6
    keySchedule[KEY_TYPE] = ENCRYPTION_KEY
    for i = 0,keyWords - 1 do
      keySchedule[i] = putByte(key[i*4+1], 3)
               + putByte(key[i*4+2], 2)
               + putByte(key[i*4+3], 1)
               + putByte(key[i*4+4], 0)
    end
    for i = keyWords, (keySchedule[ROUNDS] + 1)*4 - 1 do
      local tmp = keySchedule[i-1]
      if ( i % keyWords == 0) then
        tmp = rotWord(tmp)
        tmp = subWord(tmp)
        local index = math.floor(i/keyWords)
        tmp = bit.bxor(tmp,rCon[index])
      elseif (keyWords > 6 and i % keyWords == 4) then
        tmp = subWord(tmp)
      end
      keySchedule[i] = bit.bxor(keySchedule[(i-keyWords)],tmp)
    end
    return keySchedule
  end
  --
  -- Inverse mix column
  -- used for key schedule of decryption key
  --
  local function invMixColumnOld(word)
    local b0 = getByte(word,3)
    local b1 = getByte(word,2)
    local b2 = getByte(word,1)
    local b3 = getByte(word,0)
    return putByte(gf.add(gf.add(gf.add(gf.mul(0x0b, b1),
                         gf.mul(0x0d, b2)),
                         gf.mul(0x09, b3)),
                         gf.mul(0x0e, b0)),3)
       + putByte(gf.add(gf.add(gf.add(gf.mul(0x0b, b2),
                         gf.mul(0x0d, b3)),
                         gf.mul(0x09, b0)),
                         gf.mul(0x0e, b1)),2)
       + putByte(gf.add(gf.add(gf.add(gf.mul(0x0b, b3),
                         gf.mul(0x0d, b0)),
                         gf.mul(0x09, b1)),
                         gf.mul(0x0e, b2)),1)
       + putByte(gf.add(gf.add(gf.add(gf.mul(0x0b, b0),
                         gf.mul(0x0d, b1)),
                         gf.mul(0x09, b2)),
                         gf.mul(0x0e, b3)),0)
  end
  --
  -- Optimized inverse mix column
  -- look at http://fp.gladman.plus.com/cryptography_technology/rijndael/aes.spec.311.pdf
  -- TODO: make it work
  --
  local function invMixColumn(word)
    local b0 = getByte(word,3)
    local b1 = getByte(word,2)
    local b2 = getByte(word,1)
    local b3 = getByte(word,0)
    local t = bit.bxor(b3,b2)
    local u = bit.bxor(b1,b0)
    local v = bit.bxor(t,u)
    v = bit.bxor(v,gf.mul(0x08,v))
    w = bit.bxor(v,gf.mul(0x04, bit.bxor(b2,b0)))
    v = bit.bxor(v,gf.mul(0x04, bit.bxor(b3,b1)))
    return putByte( bit.bxor(bit.bxor(b3,v), gf.mul(0x02, bit.bxor(b0,b3))), 0)
       + putByte( bit.bxor(bit.bxor(b2,w), gf.mul(0x02, t              )), 1)
       + putByte( bit.bxor(bit.bxor(b1,v), gf.mul(0x02, bit.bxor(b0,b3))), 2)
       + putByte( bit.bxor(bit.bxor(b0,w), gf.mul(0x02, u              )), 3)
  end
  --
  -- generate key schedule for aes decryption
  --
  -- uses key schedule for aes encryption and transforms each
  -- key by inverse mix column.
  --
  local function expandDecryptionKey(key)
    local keySchedule = expandEncryptionKey(key)
    if (keySchedule == nil) then
      return nil
    end
    keySchedule[KEY_TYPE] = DECRYPTION_KEY
    for i = 4, (keySchedule[ROUNDS] + 1)*4 - 5 do
      keySchedule[i] = invMixColumnOld(keySchedule[i])
    end
    return keySchedule
  end
  --
  -- xor round key to state
  --
  local function addRoundKey(state, key, round)
    for i = 0, 3 do
      state[i + 1] = bit.bxor(state[i + 1], key[round*4+i])
    end
  end
  --
  -- do encryption round (ShiftRow, SubBytes, MixColumn together)
  --
  local function doRound(origState, dstState)
    dstState[1] =  bit.bxor(bit.bxor(bit.bxor(
          table0[getByte(origState[1],3)],
          table1[getByte(origState[2],2)]),
          table2[getByte(origState[3],1)]),
          table3[getByte(origState[4],0)])
    dstState[2] =  bit.bxor(bit.bxor(bit.bxor(
          table0[getByte(origState[2],3)],
          table1[getByte(origState[3],2)]),
          table2[getByte(origState[4],1)]),
          table3[getByte(origState[1],0)])
    dstState[3] =  bit.bxor(bit.bxor(bit.bxor(
          table0[getByte(origState[3],3)],
          table1[getByte(origState[4],2)]),
          table2[getByte(origState[1],1)]),
          table3[getByte(origState[2],0)])
    dstState[4] =  bit.bxor(bit.bxor(bit.bxor(
          table0[getByte(origState[4],3)],
          table1[getByte(origState[1],2)]),
          table2[getByte(origState[2],1)]),
          table3[getByte(origState[3],0)])
  end
  --
  -- do last encryption round (ShiftRow and SubBytes)
  --
  local function doLastRound(origState, dstState)
    dstState[1] = putByte(SBox[getByte(origState[1],3)], 3)
          + putByte(SBox[getByte(origState[2],2)], 2)
          + putByte(SBox[getByte(origState[3],1)], 1)
          + putByte(SBox[getByte(origState[4],0)], 0)
    dstState[2] = putByte(SBox[getByte(origState[2],3)], 3)
          + putByte(SBox[getByte(origState[3],2)], 2)
          + putByte(SBox[getByte(origState[4],1)], 1)
          + putByte(SBox[getByte(origState[1],0)], 0)
    dstState[3] = putByte(SBox[getByte(origState[3],3)], 3)
          + putByte(SBox[getByte(origState[4],2)], 2)
          + putByte(SBox[getByte(origState[1],1)], 1)
          + putByte(SBox[getByte(origState[2],0)], 0)
    dstState[4] = putByte(SBox[getByte(origState[4],3)], 3)
          + putByte(SBox[getByte(origState[1],2)], 2)
          + putByte(SBox[getByte(origState[2],1)], 1)
          + putByte(SBox[getByte(origState[3],0)], 0)
  end
  --
  -- do decryption round
  --
  local function doInvRound(origState, dstState)
    dstState[1] =  bit.bxor(bit.bxor(bit.bxor(
          tableInv0[getByte(origState[1],3)],
          tableInv1[getByte(origState[4],2)]),
          tableInv2[getByte(origState[3],1)]),
          tableInv3[getByte(origState[2],0)])
    dstState[2] =  bit.bxor(bit.bxor(bit.bxor(
          tableInv0[getByte(origState[2],3)],
          tableInv1[getByte(origState[1],2)]),
          tableInv2[getByte(origState[4],1)]),
          tableInv3[getByte(origState[3],0)])
    dstState[3] =  bit.bxor(bit.bxor(bit.bxor(
          tableInv0[getByte(origState[3],3)],
          tableInv1[getByte(origState[2],2)]),
          tableInv2[getByte(origState[1],1)]),
          tableInv3[getByte(origState[4],0)])
    dstState[4] =  bit.bxor(bit.bxor(bit.bxor(
          tableInv0[getByte(origState[4],3)],
          tableInv1[getByte(origState[3],2)]),
          tableInv2[getByte(origState[2],1)]),
          tableInv3[getByte(origState[1],0)])
  end
  --
  -- do last decryption round
  --
  local function doInvLastRound(origState, dstState)
    dstState[1] = putByte(iSBox[getByte(origState[1],3)], 3)
          + putByte(iSBox[getByte(origState[4],2)], 2)
          + putByte(iSBox[getByte(origState[3],1)], 1)
          + putByte(iSBox[getByte(origState[2],0)], 0)
    dstState[2] = putByte(iSBox[getByte(origState[2],3)], 3)
          + putByte(iSBox[getByte(origState[1],2)], 2)
          + putByte(iSBox[getByte(origState[4],1)], 1)
          + putByte(iSBox[getByte(origState[3],0)], 0)
    dstState[3] = putByte(iSBox[getByte(origState[3],3)], 3)
          + putByte(iSBox[getByte(origState[2],2)], 2)
          + putByte(iSBox[getByte(origState[1],1)], 1)
          + putByte(iSBox[getByte(origState[4],0)], 0)
    dstState[4] = putByte(iSBox[getByte(origState[4],3)], 3)
          + putByte(iSBox[getByte(origState[3],2)], 2)
          + putByte(iSBox[getByte(origState[2],1)], 1)
          + putByte(iSBox[getByte(origState[1],0)], 0)
  end
  --
  -- encrypts 16 Bytes
  -- key           encryption key schedule
  -- input         array with input data
  -- inputOffset   start index for input
  -- output        array for encrypted data
  -- outputOffset  start index for output
  --
  local function encrypt(key, input, inputOffset, output, outputOffset)
    --default parameters
    inputOffset = inputOffset or 1
    output = output or {}
    outputOffset = outputOffset or 1
    local state, tmpState = {}, {}
    if (key[KEY_TYPE] ~= ENCRYPTION_KEY) then
      error("No encryption key: " .. tostring(key[KEY_TYPE]) .. ", expected " .. ENCRYPTION_KEY)
      return
    end
    state = util.bytesToInts(input, inputOffset, 4)
    addRoundKey(state, key, 0)
    local round = 1
    while (round < key[ROUNDS] - 1) do
      -- do a double round to save temporary assignments
      doRound(state, tmpState)
      addRoundKey(tmpState, key, round)
      round = round + 1
      doRound(tmpState, state)
      addRoundKey(state, key, round)
      round = round + 1
    end
    doRound(state, tmpState)
    addRoundKey(tmpState, key, round)
    round = round +1
    doLastRound(tmpState, state)
    addRoundKey(state, key, round)
    util.sleepCheckIn()
    return util.intsToBytes(state, output, outputOffset)
  end
  --
  -- decrypt 16 bytes
  -- key           decryption key schedule
  -- input         array with input data
  -- inputOffset   start index for input
  -- output        array for decrypted data
  -- outputOffset  start index for output
  ---
  local function decrypt(key, input, inputOffset, output, outputOffset)
    -- default arguments
    inputOffset = inputOffset or 1
    output = output or {}
    outputOffset = outputOffset or 1
    local state, tmpState = {}, {}
    if (key[KEY_TYPE] ~= DECRYPTION_KEY) then
      error("No decryption key: " .. tostring(key[KEY_TYPE]))
      return
    end
    state = util.bytesToInts(input, inputOffset, 4)
    addRoundKey(state, key, key[ROUNDS])
    local round = key[ROUNDS] - 1
    while (round > 2) do
      -- do a double round to save temporary assignments
      doInvRound(state, tmpState)
      addRoundKey(tmpState, key, round)
      round = round - 1
      doInvRound(tmpState, state)
      addRoundKey(state, key, round)
      round = round - 1
    end
    doInvRound(state, tmpState)
    addRoundKey(tmpState, key, round)
    round = round - 1
    doInvLastRound(tmpState, state)
    addRoundKey(state, key, round)
    util.sleepCheckIn()
    return util.intsToBytes(state, output, outputOffset)
  end

  -- calculate all tables when loading this file
  calcSBox()
  calcRoundTables()
  calcInvRoundTables()

  return {
    ROUNDS = ROUNDS,
    KEY_TYPE = KEY_TYPE,
    ENCRYPTION_KEY = ENCRYPTION_KEY,
    DECRYPTION_KEY = DECRYPTION_KEY,
    expandEncryptionKey = expandEncryptionKey,
    expandDecryptionKey = expandDecryptionKey,
    encrypt = encrypt,
    decrypt = decrypt,
  }
  end)

  local buffer=_W(function(_ENV, ...)
  local function new()
    return {}
  end

  local function addString(stack, s)
    table.insert(stack, s)
  end

  local function toString(stack)
    return table.concat(stack)
  end

  return {
    new = new,
    addString = addString,
    toString = toString,
  }
  end)

  ciphermode=_W(function(_ENV, ...)
  local public = {}
  --
  -- Encrypt strings
  -- key - byte array with key
  -- string - string to encrypt
  -- modefunction - function for cipher mode to use
  --
  local random, unpack = math.random, unpack or table.unpack
  function public.encryptString(key, data, modeFunction, iv)
    if iv then
      local ivCopy = {}
      for i = 1, 16 do ivCopy[i] = iv[i] end
      iv = ivCopy
    else
      iv = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
    end
    local keySched = aes.expandEncryptionKey(key)
    local encryptedData = buffer.new()
    for i = 1, #data/16 do
      local offset = (i-1)*16 + 1
      local byteData = {string.byte(data,offset,offset +15)}
      iv = modeFunction(keySched, byteData, iv)
      buffer.addString(encryptedData, string.char(unpack(byteData)))
    end
    return buffer.toString(encryptedData)
  end
  --
  -- the following 4 functions can be used as
  -- modefunction for encryptString
  --
  -- Electronic code book mode encrypt function
  function public.encryptECB(keySched, byteData, iv)
    aes.encrypt(keySched, byteData, 1, byteData, 1)
  end

  -- Cipher block chaining mode encrypt function
  function public.encryptCBC(keySched, byteData, iv)
    util.xorIV(byteData, iv)
    aes.encrypt(keySched, byteData, 1, byteData, 1)
    return byteData
  end

  -- Output feedback mode encrypt function
  function public.encryptOFB(keySched, byteData, iv)
    aes.encrypt(keySched, iv, 1, iv, 1)
    util.xorIV(byteData, iv)
    return iv
  end

  -- Cipher feedback mode encrypt function
  function public.encryptCFB(keySched, byteData, iv)
    aes.encrypt(keySched, iv, 1, iv, 1)
    util.xorIV(byteData, iv)
    return byteData
  end

  function public.encryptCTR(keySched, byteData, iv)
    local nextIV = {}
    for j = 1, 16 do nextIV[j] = iv[j] end
    aes.encrypt(keySched, iv, 1, iv, 1)
    util.xorIV(byteData, iv)
    util.increment(nextIV)
    return nextIV
  end
  --
  -- Decrypt strings
  -- key - byte array with key
  -- string - string to decrypt
  -- modefunction - function for cipher mode to use
  --
  function public.decryptString(key, data, modeFunction, iv)
    if iv then
      local ivCopy = {}
      for i = 1, 16 do ivCopy[i] = iv[i] end
      iv = ivCopy
    else
      iv = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
    end
    local keySched
    if modeFunction == public.decryptOFB or modeFunction == public.decryptCFB or modeFunction == public.decryptCTR then
      keySched = aes.expandEncryptionKey(key)
    else
      keySched = aes.expandDecryptionKey(key)
    end
    local decryptedData = buffer.new()
    for i = 1, #data/16 do
      local offset = (i-1)*16 + 1
      local byteData = {string.byte(data,offset,offset +15)}
      iv = modeFunction(keySched, byteData, iv)
      buffer.addString(decryptedData, string.char(unpack(byteData)))
    end
    return buffer.toString(decryptedData)
  end
  --
  -- the following 4 functions can be used as
  -- modefunction for decryptString
  --
  -- Electronic code book mode decrypt function
  function public.decryptECB(keySched, byteData, iv)
    aes.decrypt(keySched, byteData, 1, byteData, 1)
    return iv
  end

  -- Cipher block chaining mode decrypt function
  function public.decryptCBC(keySched, byteData, iv)
    local nextIV = {}
    for j = 1, 16 do nextIV[j] = byteData[j] end
    aes.decrypt(keySched, byteData, 1, byteData, 1)
    util.xorIV(byteData, iv)
    return nextIV
  end

  -- Output feedback mode decrypt function
  function public.decryptOFB(keySched, byteData, iv)
    aes.encrypt(keySched, iv, 1, iv, 1)
    util.xorIV(byteData, iv)
    return iv
  end

  -- Cipher feedback mode decrypt function
  function public.decryptCFB(keySched, byteData, iv)
    local nextIV = {}
    for j = 1, 16 do nextIV[j] = byteData[j] end
    aes.encrypt(keySched, iv, 1, iv, 1)
    util.xorIV(byteData, iv)
    return nextIV
  end

  public.decryptCTR = public.encryptCTR
  return public
  end)

  -- Simple API for encrypting strings.
  --
  AES128 = 16
  AES192 = 24
  AES256 = 32
  ECBMODE = 1
  CBCMODE = 2
  OFBMODE = 3
  CFBMODE = 4
  CTRMODE = 4

  local function pwToKey(password, keyLength, iv)
    local padLength = keyLength
    if (keyLength == AES192) then
      padLength = 32
    end
    if (padLength > #password) then
      local postfix = ""
      for i = 1,padLength - #password do
        postfix = postfix .. string.char(0)
      end
      password = password .. postfix
    else
      password = string.sub(password, 1, padLength)
    end
    local pwBytes = {string.byte(password,1,#password)}
    password = ciphermode.encryptString(pwBytes, password, ciphermode.encryptCBC, iv)
    password = string.sub(password, 1, keyLength)
    return {string.byte(password,1,#password)}
  end
  --
  -- Encrypts string data with password password.
  -- password  - the encryption key is generated from this string
  -- data      - string to encrypt (must not be too large)
  -- keyLength - length of aes key: 128(default), 192 or 256 Bit
  -- mode      - mode of encryption: ecb, cbc(default), ofb, cfb
  --
  -- mode and keyLength must be the same for encryption and decryption.
  --
  function encrypt(password, data, keyLength, mode, iv)
    assert(password ~= nil, "Empty password.")
    assert(data ~= nil, "Empty data.")
    local mode = mode or CBCMODE
    local keyLength = keyLength or AES128
    local key = pwToKey(password, keyLength, iv)
    local paddedData = util.padByteString(data)
    if mode == ECBMODE then
      return ciphermode.encryptString(key, paddedData, ciphermode.encryptECB, iv)
    elseif mode == CBCMODE then
      return ciphermode.encryptString(key, paddedData, ciphermode.encryptCBC, iv)
    elseif mode == OFBMODE then
      return ciphermode.encryptString(key, paddedData, ciphermode.encryptOFB, iv)
    elseif mode == CFBMODE then
      return ciphermode.encryptString(key, paddedData, ciphermode.encryptCFB, iv)
    elseif mode == CTRMODE then
      return ciphermode.encryptString(key, paddedData, ciphermode.encryptCTR, iv)
    else
      error("Unknown mode", 2)
    end
  end
  --
  -- Decrypts string data with password password.
  -- password  - the decryption key is generated from this string
  -- data      - string to encrypt
  -- keyLength - length of aes key: 128(default), 192 or 256 Bit
  -- mode      - mode of decryption: ecb, cbc(default), ofb, cfb
  --
  -- mode and keyLength must be the same for encryption and decryption.
  --
  function decrypt(password, data, keyLength, mode, iv)
    local mode = mode or CBCMODE
    local keyLength = keyLength or AES128
    local key = pwToKey(password, keyLength, iv)
    local plain
    if mode == ECBMODE then
      plain = ciphermode.decryptString(key, data, ciphermode.decryptECB, iv)
    elseif mode == CBCMODE then
      plain = ciphermode.decryptString(key, data, ciphermode.decryptCBC, iv)
    elseif mode == OFBMODE then
      plain = ciphermode.decryptString(key, data, ciphermode.decryptOFB, iv)
    elseif mode == CFBMODE then
      plain = ciphermode.decryptString(key, data, ciphermode.decryptCFB, iv)
    elseif mode == CTRMODE then
      plain = ciphermode.decryptString(key, data, ciphermode.decryptCTR, iv)
    else
      error("Unknown mode", 2)
    end
    result = util.unpadByteString(plain)
    if (result == nil) then
      return nil
    end
    return result
  end
end

local function netSend(id, data)
  local encKey, encryptedDataPack
  if not rednet.isOpen(modemSide) then rednet.open(modemSide) end
  if clients[id] then
    encKey = tostring(id) .. "ccDHD!General_Comms*Key" .. thisCC
    encryptedDataPack = encode(encrypt(encKey, textutils.serialize({ program = "ccDialer", data = data, ccDHD = false, gStatus = gateStatus, iris = remoteIrisStatus })))
    rednet.send(id, encryptedDataPack, "ccDialerWiFi")
  end
end

local function netSendAll(data)
  if not rednet.isOpen(modemSide) then rednet.open(modemSide) end
  if clientCount < 1 then return end
  local encKey, encryptedDataPack
  for id in pairs(clients) do
    encKey = tostring(id) .. "ccDHD!General_Comms*Key" .. thisCC
    encryptedDataPack = encode(encrypt(encKey, textutils.serialize({ program = "ccDialer", data = data, ccDHD = false, gStatus = gateStatus, iris = remoteIrisStatus })))
    rednet.send(id, encryptedDataPack, "ccDialerWiFi")
  end
end

local function recordSessionData()                        --# Human readable log files (last gate & history)
  local logAddress = dialAddress or "N/A"                 --# set logAddress
  if callDirection == "none" then callDirection = "Incoming" end
  local callTime = textutils.formatTime(os.time(), false) --# format the time
  if not fs.exists("/data") then fs.makeDir("/data") end
  local previousCall = fs.open("/data/lastCall", "w")     --# record last call
  previousCall.writeLine(tostring(os.day()) .. " @ " .. callTime .. " <" .. callDirection .. "> " .. logAddress)
  previousCall.close()
end

local function updateTerminal()
  term.setCursorPos(2, 5)
  term.write("Local / Target: " .. thisGate .. " / " .. (dialAddress or "none     "))
  term.setCursorPos(14, 7)
  term.write(string.rep(" ", 20))
  term.setCursorPos(14, 7)
  term.write(gateStatus .. " " .. ((gateStatus == "Dialing" or gateStatus == "Connected") and callDirection or " "))
end

local function hangUp() --# LanteaCraft & SGCraft
  if gateStatus == "Idle" or gateStatus == "Offline" then return end
  if lcGate then
    continueDialing = false
    while spinTime do
      os.queueEvent("spinWait")
      os.pullEvent("spinWait")
    end
    pcall(gate.disengageStargate)
    chevronNumber = gate.getActivatedChevrons()
    if chevronNumber > 0 then
      for i = 1, chevronNumber do
        --[[
        local onHook = pcall(gate.deactivateChevron)
        if onHook then
          chevronNumber = chevronNumber - 1
        else
          break
        end
        ]]--
        pcall(gate.deactivateChevron)
        chevronNumber = chevronNumber - 1
      end
    end
    if chevronNumber == 0 then
      dialAddress = nil
      gateStatus = "Idle"
      remoteIrisStatus = "unk"
      netSendAll("Idle")
      updateTerminal()
    end
  else
    gate.disconnect()
  end
end

local function lockChevron(num) --# LanteaCraft
  if continueDialing then
    --[[
    local dialGate = pcall(gate.selectGlyph, dialAddress:sub(num, num))
    if not dialGate then return false end
    dialGate = pcall(gate.activateChevron)
    if not dialGate then return false end
    ]]--
    pcall(gate.selectGlyph, dialAddress:sub(num, num))
    pcall(gate.activateChevron)
    gateStatus = "Dialing"
    return true
  else
    gateStatus = "Paused"
    return false
  end
end

local function lcIrisMonitor() --# LanteaCraft
  local event --, irisStatus, dataPack
  while true do
    event = os.pullEvent()
    if event == "irisOpened" or event == "irisDestroyed" or event == "irisClosed" then
      irisState = event == "irisClosed"
      term.setCursorPos(14, 9)
      term.write(irisState and "Closed" or "Open  ")
      --if gateStatus == "Connected" then
        --irisStatus = irisState and "closed" or "open"
        --dataPack = textutils.serialize({ irisState = irisStatus })
        --pcall(gate.sendMessage, dataPack)
      --end
    end
  end
end

local function lcGateMonitor() --# LanteaCraft
  local event --, irisStatus, dataPack, establishWormhole
  while true do
    event = os.pullEvent()
    if event == "spinToGlyph" or event == "engageGlyph" or event == "connect" or event == "disconnect" then
      if event == "spinToGlyph" then
        spinTime = true
        callDirection = dialAddress and "Outgoing" or "Incoming"
      elseif event == "engageGlyph" then
        spinTime = false
        if dialAddress then
          chevronNumber = chevronNumber + 1
          if chevronNumber == #dialAddress then
            continueDialing = false
            --establishWormhole = pcall(gate.engageStargate)
            --if not establishWormhole then hangUp() end
            pcall(gate.engageStargate)
          else
            if continueDialing and not lockChevron(chevronNumber + 1) then
              continueDialing = false
              gateStatus = "Paused"
            end
          end
          netSendAll(gateStatus == "Dialing" and "Dialing" or "Paused")
        end
      elseif event == "connect" then
        gateStatus = "Connected"
        spinTime = false
        if logging then recordSessionData() end
        --irisStatus = irisState and "closed" or "open"
        --dataPack = textutils.serialize({ irisState = irisStatus })
        --pcall(gate.sendMessage, dataPack)
        netSendAll("Connected")
      elseif event == "disconnect" then
        gateStatus = "Idle"
        remoteIrisStatus = "unk"
        spinTime = false
        dialAddress = nil
        callDirection = "none"
        chevronNumber = 0
        netSendAll("Idle")
      end
      updateTerminal()
    end
  end
end

local function sgIrisMonitor() --# SGCraft
  local irisEvent, _, newState, oldState, irisStatus
  while true do
    irisEvent, _, newState, oldState = os.pullEvent("sgIrisStateChange")
    if newState == "Closed" or newState == "Open" or newState == "Offline" then
      irisState = newState == "Closed"
      term.setCursorPos(14, 9)
      term.write(irisState and "Closed" or "Open  ")
      if gateStatus == "Connected" then
        irisStatus = irisState and "closed" or "open"
        gate.sendMessage(textutils.serialize({ irisState = irisStatus }))
      end
    end
  end
end

local function sgGateMonitor() --# SGCraft
  local sgEvent, _, newState, oldState, irisStatus
  while true do
    sgEvent, _, newState, oldState = os.pullEvent("sgStargateStateChange")
    gateStatus = sgStates[newState] or "Unknown"
    if gateStatus == "Connected" then
      if not dialAddress then
        dialAddress = gate.remoteAddress()
        _, chevronNumber, callDirection = gate.stargateState()
      end
      if logging and newState ~= oldState then recordSessionData() end
      irisStatus = irisState and "closed" or "open"
      gate.sendMessage(textutils.serialize({ irisState = irisStatus }))
      netSendAll("Connected")
    elseif gateStatus == "Idle" or gateStatus == "Offline" then
      dialAddress = nil
      callDirection = "none"
      remoteIrisStatus = "unk"
      chevronNumber = 0
      netSendAll("Idle")
    end
    updateTerminal()
  end
end

local function outgoingCall() --# LanteaCraft & SGCraft
  local _, sgEvent, outgoingAddress
  while true do
    if lcGate then
      sgEvent, chevronNumber = os.pullEvent("sgChevronEncode")
    else
      sgEvent, _, outgoingAddress = os.pullEvent("sgDialOut")
    end
    gateStatus = "Dialing"
    callDirection = "Outgoing"
    updateTerminal()
    netSendAll("Dialing")
  end
end

local function incomingCall() --# LanteaCraft & SGCraft
  local _, incomingChevron, incomingAddress, sgEvent
  while true do
    if lcGate then
      _, incomingChevron = os.pullEvent("sgIncoming")
    else
      sgEvent, _, incomingAddress = os.pullEvent("sgDialIn")
    end
    gateStatus = "Dialing"
    callDirection = "Incoming"
    --if lcGate then incomingAddress = incomingAddress and incomingAddress .. incomingChevron or incomingChevron end
    dialAddress = lcGate and "N/A" or gate.remoteAddress()
    updateTerminal()
    netSendAll("Dialing")
  end
end

local function gateReceive() --# LanteaCraft & SGCraft
  local _, src, message, success, data
  while true do
    if lcGate then
      _, message = os.pullEvent("receive")
    else
      _, src, message = os.pullEvent("sgMessageReceived")
    end
    success, data = pcall(textutils.unserialize, message)
    if success and type(data) == "table" then
      if data.password then
        if irisState then
          pcall(gate.openIris)
        elseif not irisState and allowIrisClose then
          pcall(gate.closeIris)
        end
      elseif data.irisState then
        remoteIrisStatus = data.irisState
        netSendAll(remoteIrisStatus)
      end
    end
  end
end

local function netReceive()
  local id, encryptedMessage, decryptedMesesage, encodedMessage, message, success, mCommand, mcLen, dataPack
  while true do
    if not rednet.isOpen(modemSide) then rednet.open(modemSide) end
    id, encodedMessage = rednet.receive("ccDialerWiFi")
    if type(encodedMessage) == "string" then
      success, encryptedMessage = pcall(decode, encodedMessage)
      if success then
        encKey = thisCC .. "ccDHD!General_Comms*Key" .. tostring(id)
        success, decryptedMessage = pcall(decrypt, encKey, encryptedMessage)
        if success then
          success, message = pcall(textutils.unserialize, decryptedMessage)
          if success and type(message) == "table" and message.program and message.program == "ccDialer" and message.gate then
            if message.gate == "NO HOST" and message.command and message.command == "QRY" then
              if not clients[id] then clientCount = clientCount + 1 end
              clients[id] = 0
              netSend(id, thisGate)
            elseif message.gate == thisGate then
              if message.password and clients[id] and gateStatus == "Connected" then
                dataPack = textutils.serialize({ password = message.password })
                pcall(gate.sendMessage, dataPack)
              elseif message.command and clients[id] then
                mCommand = message.command
                if mCommand == "ping" then
                  netSend(id, "pong")
                elseif mCommand == "pong" then
                  clients[id] = 0
                elseif mCommand == "endCall" then
                  hangUp()
                elseif mCommand == "logout" then
                  clients[id] = nil
                  clientCount = math.max(0, clientCount - 1)
                else
                  mcLen = #tostring(mCommand)
                  if (mcLen == 7 or mcLen == 9) and mCommand ~= thisGate then
                    if lcGate then
                      if gateStatus == "Dialing" and mCommand == dialAddress then
                        continueDialing = false
                        gateStatus = "Paused"
                      elseif gateStatus == "Paused" and mCommand == dialAddress then
                        continueDialing = true
                        if not lockChevron(chevronNumber + 1) then
                          continueDialing = false
                          gateStatus = "Paused"
                        end
                      elseif gateStatus == "Idle" then
                        if gate.getActivatedChevrons() > 0 then hangUp() end
                        dialAddress = mCommand
                        continueDialing = true
                        if not lockChevron(1) then hangUp() end
                      end
                    else
                      if gateStatus == "Idle" then
                        dialAddress = mCommand
                        if not gate.dial(dialAddress) then dialAddress = nil end
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end

local function charInput()
  while true do
    local _, char = os.pullEvent("char")
    if string.lower(char) == "q" then
      netSendAll("Offline")
      rednet.unhost("ccDialerWiFi", thisGate)
      if rednet.isOpen(modemSide) then rednet.close(modemSide) end
      term.clear()
      term.setCursorPos(1, 1)
      term.write("gateBuddy is OFFLINE")
      term.setCursorPos(1, 3)
      return
    end
  end
end

local function dataPoller()
  local _, timer
  while true do
    _, timer = os.pullEvent("timer")
    if timer == pingTimer then
      if clientCount > 0 then
        for id, timeout in pairs(clients) do
          clients[id] = timeout + 1
          if clients[id] > 2 then
            clients[id] = nil
            clientCount = math.max(0, clientCount - 1)
          else
            netSend(id, "ping")
          end
        end
      end
      pingTimer = os.startTimer(5)
    end
  end 
end

local function initError(device)
  term.clear()
  term.setCursorPos(1, 1)
  term.write("No " .. device .. " detected!")
  term.setCursorPos(1, 3)
  term.write("gateBuddy is OFFLINE")
  term.setCursorPos(1, 5)
end

term.setBackgroundColor(colors.black)
term.setTextColor(colors.white)
term.clear()
term.setCursorPos(2, 2)
if pocket then error("Computer or turtle required.", 0) end
term.write("gateBuddy initializing...")
if not os.getComputerLabel() then os.setComputerLabel("gateBuddy.cc#" .. thisCC) end
gate = peripheral.find("stargate")
if not gate then
  lcGate = true
  gate = peripheral.find("StargateBase")
  if gate and not gate.isValid() then gate = nil end
end
if not gate then return initError("STARGATE") end
thisGate = lcGate and gate.getStargateAddressString() or gate.localAddress()
local irisStatus
if lcGate then
  local glyphs = gate.getActivatedGlyphs()  --# returns a string
  local chevs = gate.getActivatedChevrons() --# returns a number
  if glyphs and chevs > 0 then              --# this can be confused - if the gate is hanging up, there will be chevrons encoded, but the status is "Disconnecting" not "Dialing"
    dialAddress = chevs == 9 and glyphs or glyphs .. string.rep("?", 9 - chevs)
    gateStatus = chevs == 9 and "Connected" or "Dialing"
    chevronNumber = chevs
    callDirection = "Unknown"
  else 
    gateStatus = "Idle"
    callDirection = "none"
  end
  irisStatus = gate.getIrisState()
  irisState = irisStatus == "CLOSED"
else
  local gateState
  gateState, chevronNumber, callDirection = gate.stargateState()
  if gateState == "Offline" then return initError("STARGATE") end
  gateStatus = sgStates[gateState] or "Unknown"
  if gateStatus ~= "Idle" then dialAddress = gate.remoteAddress() end
  irisStatus = gate.irisState()
  irisState = irisStatus == "Closed"
end
for _, side in pairs(rs.getSides()) do
  if peripheral.isPresent(side) and peripheral.getType(side) == "modem" and peripheral.call(side, "isWireless") then
    modemSide = side
    rednet.open(side)
    rednet.host("ccDialerWiFi", thisGate) 
    break
  end
end
if modemSide == "none" then return initError("WIRELESS MODEM") end
if tArgs[1] then
  for i = 1, #tArgs do
    if tArgs[i] == "log" then
      logging = true
    elseif tArgs[i] == "iris" and irisStatus ~= "Offline" and irisStatus ~= "NONE" then
      allowIrisClose = true
    end
  end
end
pingTimer = os.startTimer(5)
term.clear()
term.setCursorPos(2, 2)
term.write("gateBuddy " .. gbVer .. " is ONLINE")
term.setCursorPos(2, 5)
term.write("Local / Target: " .. thisGate .. " / " .. (dialAddress or "none"))
term.setCursorPos(2, 7)
term.write("Gate State: ")
term.write(gateStatus .. " " .. ((gateStatus == "Dialing" or gateStatus == "Connected") and callDirection or " "))
term.setCursorPos(2, 9)
term.write("Iris State: " .. (irisState and "Closed" or "Open"))
term.setCursorPos(2, 12)
term.write("press 'q' to quit")
if lcGate then
  parallel.waitForAny(gateReceive, netReceive, lcGateMonitor, lcIrisMonitor, charInput, dataPoller) --incomingCall, outgoingCall,
else
  parallel.waitForAny(gateReceive, netReceive, sgGateMonitor, sgIrisMonitor, incomingCall, outgoingCall, charInput, dataPoller)
end